Les actes autorisant la création d'un traitement en application des articles 25, 26 et 27 précisent :

1° La dénomination et la finalité du traitement ;

2° Le service auprès duquel s'exerce le droit d'accès défini au chapitre VII ;

3° Les catégories de données à caractère personnel enregistrées ;

4° Les destinataires ou catégories de destinataires habilités à recevoir communication de ces données ;

5° Le cas échéant, les dérogations à l'obligation d'information prévues au V de l'article 32.
