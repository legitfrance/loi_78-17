La présente loi est applicable, dans sa rédaction résultant de la loi n° 2016-1321 du 7 octobre 2016 pour une République numérique, en Nouvelle-Calédonie, en Polynésie française, dans les îles Wallis et Futuna et dans les Terres australes et antarctiques françaises.

Par dérogation aux dispositions du cinquième alinéa du II de l'article 54, le comité d'expertise dispose d'un délai de deux mois pour transmettre son avis au demandeur lorsque celui-ci réside dans l'une de ces collectivités. En cas d'urgence, ce délai peut être ramené à un mois.

L'article 43 ter de la présente loi est applicable dans les îles Wallis  et Futuna sous réserve, au 3° du IV, de remplacer les références : “ des  articles L. 2122-1, L. 2122-5 ou L. 2122-9 du code du travail ” par les  mots : “ des articles pertinents du code du travail applicable  localement ”.
