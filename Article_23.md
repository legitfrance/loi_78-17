I. - La déclaration comporte l'engagement que le traitement satisfait aux exigences de la loi.

Elle peut être adressée à la Commission nationale de l'informatique et des libertés par voie électronique.

La commission délivre sans délai un récépissé, le cas échéant par voie électronique. Le demandeur peut mettre en oeuvre le traitement dès réception de ce récépissé ; il n'est exonéré d'aucune de ses responsabilités.

II. - Les traitements relevant d'un même organisme et ayant des finalités identiques ou liées entre elles peuvent faire l'objet d'une déclaration unique. Dans ce cas, les informations requises en application de l'article 30 ne sont fournies pour chacun des traitements que dans la mesure où elles lui sont propres.
