I. - Pour les catégories les plus courantes de traitements de données à caractère personnel, dont la mise en oeuvre n'est pas susceptible de porter atteinte à la vie privée ou aux libertés, la Commission nationale de l'informatique et des libertés établit et publie, après avoir reçu le cas échéant les propositions formulées par les représentants des organismes publics et privés représentatifs, des normes destinées à simplifier l'obligation de déclaration.

Ces normes précisent :

1° Les finalités des traitements faisant l'objet d'une déclaration simplifiée ;

2° Les données à caractère personnel ou catégories de données à caractère personnel traitées ;

3° La ou les catégories de personnes concernées ;

4° Les destinataires ou catégories de destinataires auxquels les données à caractère personnel sont communiquées ;

5° La durée de conservation des données à caractère personnel.

Les traitements qui correspondent à l'une de ces normes font l'objet d'une déclaration simplifiée de conformité envoyée à la commission, le cas échéant par voie électronique.

II. - La commission peut définir, parmi les catégories de traitements mentionnés au I, celles qui, compte tenu de leurs finalités, de leurs destinataires ou catégories de destinataires, des données à caractère personnel traitées, de la durée de conservation de celles-ci et des catégories de personnes concernées, sont dispensées de déclaration.

Dans les mêmes conditions, la commission peut autoriser les responsables de certaines catégories de traitements à procéder à une déclaration unique selon les dispositions du II de l'article 23.
