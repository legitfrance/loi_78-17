Les dispositions de la présente loi ne font pas obstacle à l'application, au bénéfice de tiers, des dispositions du livre III du code des relations entre le public et l'administration et des dispositions du livre II du code du patrimoine.

En conséquence, ne peut être regardé comme un tiers non autorisé au sens de l'article 34 le titulaire d'un droit d'accès aux documents administratifs ou aux archives publiques exercé conformément au livre III du code des relations entre le public et l'administration et au livre II du code du patrimoine.
